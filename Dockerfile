FROM anapsix/alpine-java
MAINTAINER JSchwarz
COPY maid-redirect-1.0-SNAPSHOT.jar /home/maid-redirect.jar
EXPOSE 8080/tcp
CMD ["java","-jar","/home/maid-redirect.jar"]