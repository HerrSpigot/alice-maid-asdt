package de.alicehospital.maid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Core {

    private static String version = "0.0.01a";


    public static void main(String[] args) {
        SpringApplication.run(Core.class, args);
    }


    public static String getVersion() {
        return version;
    }

}
