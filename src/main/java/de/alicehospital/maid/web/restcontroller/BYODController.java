package de.alicehospital.maid.web.restcontroller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BYODController {

    @RequestMapping("/.well-known/com.apple.remotemanagement")
    public ResponseEntity<JsonNode> get() {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = null;
        try {
            json = mapper.readTree("{\"Servers\":[{\"BaseURL\":\"https://mdm.alice-hospital.de/.well-known/ueenroll?userIdentifier=Unknown\",\"Version\":\"mdm-byod\"}]}");
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok(json);
    }

}
